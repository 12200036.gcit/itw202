import React, { Component } from 'react'
import { Text, View, Button } from 'react-native'
class CountClass extends Component {
    state = { count: 0}
    onPress = () => {
        this.setState({
        count: this.state.count + 1})}
        render() {
            return (
                <View><Text>you clicked { this.state.count } time </Text>
                <Button
                 onPress={ this.onPress }
                 title="Count"/> 
                 </View>)}}
export default CountClass;
