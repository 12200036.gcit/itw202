import React from 'react'
import {Image,View} from 'react-native'
import 'react-native-gesture-handler'
import HomeScreen from '../src/Screens/HomeScreen'
import ProfileScreen from '../src/Screens/ProfileScreen'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

const Tab=createBottomTabNavigator();

export default function LowNavigation(){
    return(
      <Tab.Navigator>
        <Tab.Screen name='Home' component={HomeScreen}
          options={{
            tabBarIcon:({size})=>{
              return(
                <Image
                  style={{width:size,height:size}}
                  source={require('../assets/home-icon.png')}
                />
              )
            }
          }}
        />
        <Tab.Screen name='Profile' component={ProfileScreen}
           options={{
            tabBarIcon:({size})=>{
              return(
                <Image
                  style={{width:size,height:size}}
                  source={require('../assets/setting-icon.png')}
                />
              )
            }
          }}
        />
      </Tab.Navigator>
    )
  }
  
