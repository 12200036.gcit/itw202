import React,{useState} from 'react'
import {TextInput,StatusBar, View, StyleSheet,Alert,Text} from 'react-native'
import Colors from '../constants/Colors'
import PrimaryButton from '../components/ui/PrimaryButton'
import Title from '../components/ui/Title'
import Card from '../components/ui/Card'
import InstructionText from '../components/ui/InstructionText'
function StartGameScreen({onPickNumber}) {

  const [enterNumber,setEnterNumber]=useState('')
  
  const numberInputHandler=(enterNumber)=>{
    setEnterNumber(enterNumber)
  }
  const resetInputHandler=()=>{
    setEnterNumber('')
  }

  const confirmInputHandler=()=>{
    const chosenNumber=parseInt(enterNumber)
    if(isNaN(chosenNumber)|| chosenNumber<=0 || chosenNumber>99){
      Alert.alert('Invalid Number!','Number has to be a number between 1 and 99',
      [{text:'Okay',style:'destructive',onPress:resetInputHandler}]
      )
      return;  
  }
   onPickNumber(chosenNumber) 
  // console.log("valid number")
  }
  
  return (
    <View style={styles.rootContainer}>
      <StatusBar style='auto'/>
      <Title>Guess My Number</Title>
      <Card>
        <View style={styles.inputContainer}>
          <InstructionText>Enter a Number</InstructionText>
          <TextInput
          style={styles.numberInput}
          keyboardType='number-pad'
          maxLength={2}
          autoCapitalize='none'
          autoCorrect={false}
          value={enterNumber}
          onChangeText={numberInputHandler}
          />
          <View style={styles.buttonsContainer}>
            <View style={styles.buttonContainer}>
                <PrimaryButton onPress={resetInputHandler}>Reset</PrimaryButton>
            </View>
            <View style={styles.buttonContainer}>
            <PrimaryButton onPress={confirmInputHandler}>Confirm</PrimaryButton>
            </View>
          </View>
        </View>
      </Card>
      
 
    </View>
  )
}
export default StartGameScreen
const styles = StyleSheet.create({
  rootContainer:{
    paddingVertical:50,
    alignItems:'center',
    height:'100%',
  },
  inputContainer: {
    justifyContent: 'center',
    alignItems:'center',
    marginTop: 36,
    marginHorizontal: 24,
    padding: 16,
    backgroundColor: '#72063C',
    borderRadius: 8,
    elevation: 4,
    shadowColor: 'black',
    shadowOffset: {width: 0, height: 2},
    shadowRadius: 6,
    shadowOpacity: 0.25,
    borderWidth:1,
    borderColor:'white'
  },
  numberInput: {
    height: 50,
    width: 50,
    fontSize: 32,
    borderBottomColor:'#DDB52F',
    borderBottomWidth: 2,
    color: '#DDB52F',
    marginVertical: 8,
    fontWeight: 'bold',
    textAlign: 'center'
},
buttonsContainer: {
    flexDirection: 'row'
},
buttonContainer: {
    flex: 1,
},
})