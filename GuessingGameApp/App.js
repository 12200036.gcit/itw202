import React, { useState } from 'react';
import { StyleSheet, View,SafeAreaView,Button,Text } from 'react-native';

import GameScreen from './Screens/GameScreen';
import StartGameScreen from './Screens/StartGameScreen';
import GameOverScreen from './Screens/GameOverScreen';
import AppLoading from 'expo-app-loading'
import { useFonts } from 'expo-font';

export default function App() {
  const [userNumber,setUserNumber]=useState()
  const [gameIsOver,setGameIsOver]=useState(true)
  const [guessRounds,setGuessRounds]=useState(0)


  const [fontsLoaded]=useFonts({
    'open-sans':require('./assets/fonts/OpenSans-Regular.ttf'),
    'open-sans-bold':require('./assets/fonts/OpenSans-Bold.ttf')
  })
  if(!fontsLoaded){
    return <AppLoading/>
  }

  function pickedNumberHandler(pickNumber){
    setUserNumber(pickNumber)
    setGameIsOver(false)
   
  }
  function gameOverHandler(numberOfRounds){
    setGameIsOver(true)
    setGuessRounds(numberOfRounds)
  }
  function startNewGameHandler(){
    setUserNumber(null)
    setGuessRounds(0)
  }
  
  

  let screen=<StartGameScreen onPickNumber={pickedNumberHandler}/>

  if(userNumber){
    screen=<GameScreen userNumber={userNumber} onGameOver={gameOverHandler}/>
  }
  if(gameIsOver && userNumber){
    screen=<GameOverScreen 
            userNumber={userNumber}
            roundsNumber={guessRounds}
            onStartNewGame={startNewGameHandler}
    />
  }
  return (
    <View style={styles.container}>
      <SafeAreaView>
        {screen}
      </SafeAreaView>
    
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#4c0329',
    // alignItems: 'center',
    // justifyContent: 'center',
  },
});
