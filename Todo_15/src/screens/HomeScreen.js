import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import Background from '../component/Background'
import Header from '../component/Header'
import Button from '../component/Button'
import { logoutUser } from '../api/auth-api'


const HomeScreen = ({navigation}) => {
  return (
     <Background>
        <Header>Home</Header>
        <Button mode='contained' 
          onPress={()=>{
            logoutUser()
          }}
        >Logout</Button>
    </Background>
  )
}

export default HomeScreen

const styles = StyleSheet.create({})