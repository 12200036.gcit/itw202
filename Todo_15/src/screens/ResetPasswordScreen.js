import {StatusBar } from 'react-native'
import React, {useState}from 'react'
import Header from '../component/Header'
import Button from '../component/Button'
import Background from '../component/Background'
import Logo from '../component/Logo'
import TextInput from '../component/TextInput'
import { emailValidator } from '../core/helpers/emailValidator'
import BackButon from '../component/BackButton'
import { sendEmailWithPassword } from '../api/auth-api'
const ResetPasswordScreen = ({navigation}) => {
    const [email,setEmail]=useState({value:'',error:''})
    const [loading,setLoading]=useState()

    const onSubmitPressed=async()=>{
      const emailError=emailValidator(email.value)
      if(emailError){
        setEmail({...email,error:emailError})
      }
      setLoading(true)
      const response=await sendEmailWithPassword(email.value)
      if(response.error){
        alert(response.error)
      }
      else{
        alert("Email with password has been sent.")
      }
      setLoading(false)
    }
  return (
    <Background>
      <StatusBar style='auto'/>
      <BackButon goBack={navigation.goBack}/>
      <Logo/>
      <Header>Restore Password</Header>
      <TextInput 
        label="Email"
        value={email.value}
        error={email.error}
        errorText={email.error}
        onChangeText={(text)=>setEmail({value:text,error:""})}
        description="You will receive email with password reset link."
      />
      <Button loading={loading} mode='contained' onPress={onSubmitPressed} >Send Instructions</Button>
    </Background>
  )
}

export default ResetPasswordScreen
