import { StyleSheet, Text, View,StatusBar,TouchableOpacity } from 'react-native'
import React, {useState}from 'react'
import Header from '../component/Header'
import Button from '../component/Button'
import Background from '../component/Background'
import Logo from '../component/Logo'
import TextInput from '../component/TextInput'
import { emailValidator } from '../core/helpers/emailValidator'
import { passwordValidator } from '../core/helpers/passwordValidator'
import BackButon from '../component/BackButton'
import  Paragraph  from '../component/Paragraph'
import { nameValidation } from '../core/helpers/nameValidation'
import { signUpUser } from '../api/auth-api'

const SignupScreen = ({navigation}) => {
    const [email,setEmail]=useState({value:'',error:''})
    const [password,setPassword]=useState({value:'',error:''})
    const [name,setName]=useState({value:'',error:''})
    const [loading,setLoading]=useState()

    const onSignupPressed=async()=>{

      const emailError=emailValidator(email.value)
      const passwordError=passwordValidator(password.value)
      const nameError=nameValidation(name.value)

      if(emailError || passwordError || nameError){
        setEmail({...email,error:emailError})
        setPassword({...password,error:passwordError})
        setName({...name,error:nameError})
      }
      setLoading(true)
      const response=await signUpUser({
        name:name.value,
        email:email.value,
        password:password.value
      })
      if(response.error){
        alert(response.error)
      }
      else{
        navigation.replace('HomeScreen')
        // console.log(response.user.displayName)
        // alert(response.user.displayName)
      }
      setLoading(false)
    }
  return (
    <Background>
      <StatusBar style='auto'/>
      <BackButon goBack={navigation.goBack}/>
      <Logo/>
      <Header>Create Account</Header>
      <TextInput 
        label="Name"
        error={name.error}
        errorText={name.error}
        value={name.value}
        onChangeText={(text)=>setName({value:text,error:''})}
      />
      <TextInput 
        label="Email"
        value={email.value}
        error={email.error}
        errorText={email.error}
        onChangeText={(text)=>setEmail({value:text,error:""})}
      />
      <TextInput 
        secureTextEntry 
        label="Password"
        value={password.value}
        error={password.error}
        errorText={password.error}
        onChangeText={(text)=>setPassword({value:text,error:""})}
      />

      <Button loading={loading} mode='contained' onPress={onSignupPressed} >Sign up</Button>
      <View style={styles.row}>
        <Paragraph>Already have an account? </Paragraph>
        <TouchableOpacity>
          <Text style={styles.signupText} onPress={()=>navigation.replace("LoginScreen")}>Log In</Text>
        </TouchableOpacity> 
      </View>
    </Background>
  )
}

export default SignupScreen

const styles = StyleSheet.create({
  row:{
    flexDirection:'row',
    justifyContent:'center'
  },
  signupText:{
   fontWeight:'bold'
  }
})