import React from 'react'
import { ActivityIndicator } from 'react-native'
import Background from '../component/Background'
import firebase from 'firebase/app'
const AuthLoadingScreen = ({navigation}) => {
    firebase.auth().onAuthStateChanged((user)=>{
        if(user){
            //user is logged in
            navigation.reset({
                routes:[{name:"HomeScreen"}]
            });
        }
        else{
            //user is not logged in
            navigation.reset({
                routes:[{name:'StartScreen'}]
            })
        }
    });
  return (
    <Background>
        <ActivityIndicator size='large' />
    </Background>
  )
}

export default AuthLoadingScreen

