import { StyleSheet, Text, View,Button } from 'react-native'
import React from 'react'
const Screen1 = ({navigation}) => {
  return (
    <View>
      <Text>login</Text>
      <Button title='Navigate to Screen2' onPress={()=>navigation.navigate('Screen2')}/>

      <Button
        title='reset to screen3' onPress={()=>navigation.reset({
          index:0,
          routes:[{name:'Screen3'}]
        })}
      />
    </View>
  )
}


export default Screen1

const styles = StyleSheet.create({})