import { StyleSheet, Text, View,Button } from 'react-native'
import React from 'react'

const Screen2 = ({navigation}) => {
  return (
    <View>
      <Text>Screen2</Text>
      <Button
        title='go back'
        onPress={()=>navigation.goBack()}
      />
    </View>
  )
}

export default Screen2

const styles = StyleSheet.create({})